/**
 *  @author : Tuan Lai 
 *  @Project :   Test Cafe - Javascript UI automation
 *  @description : 
 * 
 *          - Tc1 : Add a product to cart
                    Story Phase
                    As a customer
                    I want to go to a product page and add the product to the cart
                    I should be able to see this product added to cart on shopping cart page
           
             - Tc2 : See a Product on Quick Look Overlay
                    Story Phase
                    As a customer
                    I want to search for a product and open it's quick look overlay
                    I should be able to see the exact same clicked product on the quick look overlay
        
            Note: Browser sometime hanging after quicklook overlay was dispalyed

 */


import { getJsonTestData } from '../helper/util';
import { environment } from '../helper/util';
import AccountPage from "../pages/common/account_page";
import Homepage from '../pages/common/homepage';
import ProductResultPage from '../pages/product/product_result_page';
import ProductPage from '../pages/product/product_page';
import ShoppingCartPage from '../pages/common/shopping_cart_page';

const accountPage = new AccountPage();
const homePage = new Homepage();
const productResultPage = new ProductResultPage();
const productPage = new ProductPage();
const shoppingCartPage = new ShoppingCartPage();

const email = "suv3700@gmail.com";
const password = "Addprod2cart";
const productName = "French Chef Salt & Pepper Mill Set";
const mainCategory = "COOKS' TOOLS";
const subCategory =  "Salt & Pepper Mills"; 
const productPrice = "$89.95";


fixture `addProduct`
.page (`${environment}`)
.beforeEach( async t => {
    await t.maximizeWindow();
    const key = t.testRun.test.meta.DataKey;
    if(key != undefined) {
        t.ctx.testData = getJsonTestData(key);
    } 
    await homePage.closePopupAd();  //close popup ad from the homepage 
});
  

//Verify user can add a product to cart
test.meta('TestCaseID', 'JIRA-0002')
.meta({regression:'regression'})
("Williams Sonoma - Add Product - Add a product to cart", 
async t => {
 
    //Sign in as a customer 
    await accountPage.signin(email, password);
    
    //Go to product page and add a product to cart
    await homePage.navigation.selectProductCategoryByName(mainCategory, subCategory);

    //From the product result apge , click on a product by name 
    await productResultPage.clickProductByName(productName);

    //From the product page, add the product to cart
    await productPage.addToCart();

    //From the add to cart overlay, click on checkout button
    await productPage.addToCartOverlay.clickCheckoutBtn();

    //Verify added product should be on shopping cart page
    await t.expect(await shoppingCartPage.isCheckoutItemExist(productName)).ok(`${productName} should be one of the checkout item in shopping cart page`);

});

//Verify search product is displayed in quick look overlay
test.meta('TestCaseID', 'JIRA-0002')
.meta({smoke:'smoke', regression:'regression'})
("Williams Sonoma - Quick Look - Verify search product is displayed", 
async t => {

    //Sign in as a customer 
    await accountPage.signin(email, password);

    //Go to product page and add a product to cart
    await homePage.search.searchByKeyword(productName);

    //From the product result page click on product quicklook link
    await productResultPage.clickProductQuickLookByName(productName);
  
    //Verify quicklook overlay display correct product name and price
    await t.expect(await productResultPage.quickLookOverlay.prodSummaryHeader.innerText)
            .eql(productName, `${productName} should be displayed on quicklook overlay`);
    await t.expect(await productResultPage.quickLookOverlay.prodPrice.innerText)
            .eql(productPrice, `${productPrice} should be displayed on quicklook overlay`);

});