import { ClientFunction } from 'testcafe';
import minimist from 'minimist';

 
const args = minimist(process.argv.slice(2));


/**
 * Retrieve base url from the env parameter during test run 
 */
export const environment = args.env;

/**
 * Client Function that returns the current url
 */
export const pageUrl = ClientFunction(() => window.location.href);
 