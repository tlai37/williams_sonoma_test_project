import { Selector, t} from 'testcafe';
import AddToCartOverlay from '../components/add_to_cart_component'

export default class ProductPage {
    constructor(){
        //Web Objects
        this.addToCartBtn = Selector('.add-to-basket');
        this.productName = Selector('.pip-summary h1');
        this.addToCartOverlay = new AddToCartOverlay();
    }

    /**
     * Click product by name 
     * @param {string} productName - Product Name 
     */
    async addToCart(productName){
        await t.click(await this.addToCartBtn);
     }    
 

}