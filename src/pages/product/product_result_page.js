import { Selector, t} from 'testcafe';
import QuickLookOverlay from '../components/quick_look_component';
import Navigation from "../components/navigation_component";
import Search from '../components/search_component';


export default class ProductResultPage {
    constructor(){
        //Web Objects
        this.breadcrumbHeader = Selector('.breadcrumb-list-heading');
        this.productNameLinks = Selector('.product-cell .product-name');

        this.quickLookOverlay = new QuickLookOverlay();
        this.navigation = new Navigation();
        this.search = new Search();
    }

    /**
     * Click product by product name 
     * @param {string} productName - Product Name 
     */
    async clickProductByName(productName){
        await t.click(await this.productNameLinks.withText(productName));
     }    
 
    /**
     * Click product quick look by product name 
     * @param {string} productName - Product Name 
     */
    async clickProductQuickLookByName(productName){
        await t.click(await this.productNameLinks.withText(productName).parent('li').find('.quicklook-link'));
     }    
 
     

}