import { Selector, t} from 'testcafe';

export default class AddToCartOverlay {
    constructor(){
        //Web Objects
        this.prodTitle = Selector('#racOverlay #title');
        this.checkoutBtn = Selector('#racOverlay #anchor-btn-checkout'); 
    }
  
    /**
     * Click checkout button
     * @param {string} productName - Product Name 
     */
    async clickCheckoutBtn(){
        await t.click(await this.checkoutBtn);
     }    
 
}