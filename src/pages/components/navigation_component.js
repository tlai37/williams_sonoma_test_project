import { Selector, t} from 'testcafe';

export default class Navigation {
    constructor(){
        //Web Objects
        this.navItems = Selector('#topnav-container .dropDown>a');
        this.subNavItems = Selector('#topnav-container .dropDown a');
        this.keyRewardsLink = Selector('.key-rewards-link');
        this.trackYourOrderLink = Selector('.track-order-link'); 
        this.checkoutLink = Selector('.cart-button');
    }

    
    /**
     * Select Navigation menu by clicking on main category and sub category by name 
     * @param {string} categoryName - Main category name
     * @param {string} subCategoryName - Subcategory name
    */
    async selectProductCategoryByName(categoryName, subCategoryName){
        await t.hover(await this.navItems.withText(categoryName))
                .click(await this.subNavItems.withText(subCategoryName));
     }    
 

 
}