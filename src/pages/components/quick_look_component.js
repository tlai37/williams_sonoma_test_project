import { Selector, t} from 'testcafe';

export default class QuickLookOverlay {
    constructor(){
        //Web Objects
        this.prodSummaryHeader = Selector('#quicklookOverlay h1');
        this.prodPrice = Selector('#quicklookOverlay .product-price'); 
    }
  
}