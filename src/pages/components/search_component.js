import { Selector, t} from 'testcafe';

export default class Search {
    constructor(){
        //Web Objects
        this.searchboxInput = Selector('#nav-search-box #search-field');
        this.searchboxButton = Selector ('#nav-search-box #btnSearch');
    }
    
    /**
     * Select Navigation menu by clicking on main category and sub category by name 
     * @param {string} keyword - search keyword
     * @param {string} subCategoryName - Subcategory name
    */
    async searchByKeyword(keyword){
        await t.typeText(await this.searchboxInput, keyword);
        await t.click(await this.searchboxButton);
               
    }    
 

 
}