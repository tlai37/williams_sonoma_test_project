import { Selector, t} from 'testcafe';
import Navigation from "../components/navigation_component";
import Search from '../components/search_component';


export default class Homepage {
    constructor(){
        //Web Objects
        this.joineEmailAd = Selector('.joinEmailList');
        this.adCampaignCloseBtn = Selector('.stickyOverlayMinimizeButton');
        this.homePageLink = Selector('..tab-williams-sonoma-home');
        this.myAccountLink = Selector('.my-account');
        this.keyRewardsLink = Selector('.key-rewards-link');
        this.trackYourOrderLink = Selector('.track-order-link'); 
        this.checkoutLink = Selector('.cart-button');
        
        this.navigation = new Navigation();
        this.search = new Search();
    }

    /**
    * If popup ad if display, then close the ad
    */
    async closePopupAd(){
        const joinEmailAd = await this.joineEmailAd();
        if (await this.joineEmailAd.exists){
         await t.click(await this.adCampaignCloseBtn);
       }
    }       
 
}