import { Selector, t} from 'testcafe';

export default class shoppingCartPage {
    constructor(){
        //Web Objects
        this.checkoutItems = Selector('.shipping-and-delivery-details .cart-table-row-title a');
         
    }

    /**
     * Get checkout item by product name 
     * @param {string} productName - Product Name 
     * @returns {boolean} - checkout item exist (true/false)
     */
    async isCheckoutItemExist(productName){
        const checkoutItem = await this.checkoutItems.withText(productName);    
        return checkoutItem.exists;    
     }    
 

}