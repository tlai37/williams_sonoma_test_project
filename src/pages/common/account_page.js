import { Selector, t} from 'testcafe';
import Homepage from './homepage';

export default class AccountPage extends Homepage{
    constructor(){
        super();
        //Web Objects
        this.emailtInput = Selector('#login-email');
        this.passwordInput = Selector('.accountLoginForm #login-password');
        this.signInBtn = Selector('.accountLoginForm #btn-sign-in');         
    }

     /**
     * signin account by email and password 
     * @param {string} email - Email 
     * @param {string} pwd - Password 
    */
   async signin(email, password){
        await t.click(this.myAccountLink);
        await t.typeText(this.emailtInput, email);
        await t.typeText(this.passwordInput, password)
        await t.click(this.signInBtn);
    }    
    
}