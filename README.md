# williams-sonoma Test Project

**Installation** 



1. Download Window Studio code (source code editor)

* https://code.visualstudio.com/download 

2. Install Node js

* https://nodejs.org/en/ 

3. Install testcafe ( https://devexpress.github.io/testcafe/ )
  
     **TestCafe can be used writing test automation in Java script or type script without selenium driver**

     see: https://testcafe-discuss.devexpress.com/t/why-not-use-selenium-how-to-use-testcafe/47

*       npm install -g testcafe 

4. Clone this project to your local C Drive:

*git clone https://gitlab.com/tlai37/williams_sonoma_test_project.git*
 

**Instructions**

1. Open Visual Studio Code 

2. Locate the Project, and Open C:\williams_sonoma_test_project\Package.json 

    - Notice it is set to run the entire suite as "regression" :
       
3. Click on "Terminal - New Terminal", at the top of the folder type the following to run scripts:

   Perform the following steps: 
*     npm install
*     npm test (Run regression test by default)

Result :
    TC1 - Add a product to cart = PASS
    Tc2 : See a Product on Quick Look Overlay = FAIL
        - Quicklook overlay often time hangiing during the test run (site issue)     
        

**Test Cafe Framework** 

* Benefits 
	- Easy to install 
	- Do not need Selenium WebDriver or other testing software to write UI automation test


* Page Objects 
	- All the web objects are organized by common pages, components, category)
	- Components can be used by common pages or category pages:		
		- Homepage has access to all web objects and function to navigation and search components
		- Product Result page has access to quick look, navigation and search components 
 

- Execute automation tests:
	- Testcafe can be run on Chrome, FireFox, Edge etc. (testcafe 'chrome')
	- It can run concurrently/in parallell  ( -c 1 or -c 10 )
	- Test script can be tag as "smoke" or "regression" (--test-meta regression=regression)
	- Environment can be passed in during the test run (--env=https://www.williams-sonoma.com/)
	- During test run , testcafe also caugh javascript error from the website, therefore add --skip-js-errors to ignore Javascript error from the code

- By default, package.json is set to run regression  

	"test": "testcafe 'firefox' -c 1 ./src/tests/ --test-meta regression=regression --env=https://www.williams-sonoma.com/ --skip-js-errors"
